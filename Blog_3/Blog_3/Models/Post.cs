﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_V3.Models
{
    public class Post
    {
        public int ID { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 20-500", MinimumLength = 20)]
        public String Title { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String Body { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.Date)]
        public DateTime DateUpdate { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }
        public int AccountID { set; get; }
        //1 Post co nhieu comment
        public virtual ICollection<Comment> Comments { set; get; }
        //1 Post co nhieu Tag
        public virtual ICollection<Tag> Tags { set; get; }
    }
}