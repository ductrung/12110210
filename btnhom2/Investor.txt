Website tra cứu thông tin du lịch
Tên: Lê Nhất Sinh
MSSV: 12110164

Lý do đầu tư vào website này:

- Website tập trung hướng đến người dùng, đưa lợi ích của người dùng lên hàng đầu, mang lại cho người dùng cảm giác tin cậy và thoải mái khi truy cập thông tin từ website.

- Website luôn được cập nhập liên tục chính vì vậy mà luôn luôn thu hút được các đối tượng người dùng.

- Ngoài ra Website có phần liên kết với các doanh nghiệp, thông qua website thì người dùng sẽ biết đến doanh nghiệp nhiều hơn và việc quảng cáo online trở nên dễ dàng hơn.

- Tóm lại việc đầu tư vào website rất ít tốn kém nhưng đó lại là một thị trường tiềm năng để có thể đầu tư sinh lãi trong thời gian lâu dài.
