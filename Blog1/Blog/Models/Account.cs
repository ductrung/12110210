﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Account
    {
        public int AccountID { set; get; }

        [Required(ErrorMessage="Dữ liệu chưa được nhập")]
        [DataType(DataType.Password)]
        public String PassWord { set; get; }

        [Required(ErrorMessage="Dữ liệu chưa được nhập.")]
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }

        [Required(ErrorMessage="Dữ liệu chưa được nhập")]
        [StringLength(100, ErrorMessage = "Số lượng ký tự tối đa là 100")]
        public String FirstName { set; get; }

        [Required(ErrorMessage="Dữ liệu chưa được nhập.")]
        [StringLength(100, ErrorMessage = "Số lượng ký tự tối đa là 100")]
        public String LastName { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
        //public virtual ICollection<Comment> Comments { set; get; }

    }
}