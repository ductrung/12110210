﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    [Table("BAIVIET")]
    public class Post
    {
        [Range(2,100)]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500,ErrorMessage="Số lượng ký tự từ 20-500",MinimumLength=20)]
        public String Title { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500,ErrorMessage="Số lượng ký tự tối thiểu là 50",MinimumLength=50)]
        public String Body { set; get; }


        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdate { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        public int AccountID { set; get; }
        public virtual Account Accounts { set; get; }

    }
}