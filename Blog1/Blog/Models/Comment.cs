﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 50)]
        public String Body { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdate { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        public String Author { set; get; }

        public int PostID { set; get; }
        public virtual Post Posts { set; get; }
        //public int AccountID { set; get; }
        //public virtual Account Accounts { set; get; }
    }
}