namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        PassWord = c.String(nullable: false),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.BAIVIET", "AccountID", c => c.Int(nullable: false));
            AlterColumn("dbo.BAIVIET", "ID", c => c.Int(nullable: false));
            AddForeignKey("dbo.BAIVIET", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
            CreateIndex("dbo.BAIVIET", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BAIVIET", new[] { "AccountID" });
            DropForeignKey("dbo.BAIVIET", "AccountID", "dbo.Accounts");
            AlterColumn("dbo.BAIVIET", "ID", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.BAIVIET", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
