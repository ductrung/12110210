namespace Blog.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BAIVIET", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false, maxLength: 500));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "Body", c => c.String(maxLength: 500));
            AlterColumn("dbo.BAIVIET", "Body", c => c.String(maxLength: 500));
        }
    }
}
