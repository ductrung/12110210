﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_final.Models;

namespace Project_final.Controllers
{
    [Authorize]
    public class BaiHocController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /BaiHoc/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var baihocs = db.BaiHocs.Include(b => b.LoaiBaiHocs);
            return View(baihocs.ToList());
        }

        //
        // GET: /BaiHoc/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            BaiHoc baihoc = db.BaiHocs.Find(id);
            ViewData["idbaihoc"] = id;
            if (baihoc == null)
            {
                return HttpNotFound();
            }
            return View(baihoc);
        }

        //
        // GET: /BaiHoc/Create

        public ActionResult Create()
        {
            ViewBag.LoaiBaiHocID = new SelectList(db.LoaiBaiHocs, "ID", "LoaiBH");
            return View();
        }

        //
        // POST: /BaiHoc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BaiHoc baihoc)
        {
            if (ModelState.IsValid)
            {
                baihoc.NgayDang = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                baihoc.UserProfileID = userid;
                db.BaiHocs.Add(baihoc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LoaiBaiHocID = new SelectList(db.LoaiBaiHocs, "ID", "LoaiBH", baihoc.LoaiBaiHocID);
            return View(baihoc);
        }

        //
        // GET: /BaiHoc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BaiHoc baihoc = db.BaiHocs.Find(id);
            if (baihoc == null)
            {
                return HttpNotFound();
            }
            ViewBag.LoaiBaiHocID = new SelectList(db.LoaiBaiHocs, "ID", "LoaiBH", baihoc.LoaiBaiHocID);
            return View(baihoc);
        }

        //
        // POST: /BaiHoc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BaiHoc baihoc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(baihoc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LoaiBaiHocID = new SelectList(db.LoaiBaiHocs, "ID", "LoaiBH", baihoc.LoaiBaiHocID);
            return View(baihoc);
        }

        //
        // GET: /BaiHoc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BaiHoc baihoc = db.BaiHocs.Find(id);
            if (baihoc == null)
            {
                return HttpNotFound();
            }
            return View(baihoc);
        }

        //
        // POST: /BaiHoc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BaiHoc baihoc = db.BaiHocs.Find(id);
            db.BaiHocs.Remove(baihoc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}