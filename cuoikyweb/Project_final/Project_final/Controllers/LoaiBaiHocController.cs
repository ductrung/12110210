﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_final.Models;

namespace Project_final.Controllers
{
    public class LoaiBaiHocController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /LoaiBaiHoc/

        public ActionResult Index()
        {
            return View(db.LoaiBaiHocs.ToList());
        }

        //
        // GET: /LoaiBaiHoc/Details/5

        public ActionResult Details(int id = 0)
        {
            LoaiBaiHoc loaibaihoc = db.LoaiBaiHocs.Find(id);
            if (loaibaihoc == null)
            {
                return HttpNotFound();
            }
            return View(loaibaihoc);
        }

        //
        // GET: /LoaiBaiHoc/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /LoaiBaiHoc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LoaiBaiHoc loaibaihoc)
        {
            if (ModelState.IsValid)
            {
                db.LoaiBaiHocs.Add(loaibaihoc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(loaibaihoc);
        }

        //
        // GET: /LoaiBaiHoc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LoaiBaiHoc loaibaihoc = db.LoaiBaiHocs.Find(id);
            if (loaibaihoc == null)
            {
                return HttpNotFound();
            }
            return View(loaibaihoc);
        }

        //
        // POST: /LoaiBaiHoc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LoaiBaiHoc loaibaihoc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(loaibaihoc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(loaibaihoc);
        }

        //
        // GET: /LoaiBaiHoc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LoaiBaiHoc loaibaihoc = db.LoaiBaiHocs.Find(id);
            if (loaibaihoc == null)
            {
                return HttpNotFound();
            }
            return View(loaibaihoc);
        }

        //
        // POST: /LoaiBaiHoc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LoaiBaiHoc loaibaihoc = db.LoaiBaiHocs.Find(id);
            db.LoaiBaiHocs.Remove(loaibaihoc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}