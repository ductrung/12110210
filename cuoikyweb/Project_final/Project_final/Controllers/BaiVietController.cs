﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_final.Models;

namespace Project_final.Controllers
{
    [Authorize]
    public class BaiVietController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /BaiViet/
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View(db.BaiViets.ToList());
        }

        //
        // GET: /BaiViet/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            ViewData["idbaiviet"] = id;
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /BaiViet/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BaiViet baiviet,String Content)
        {
            if (ModelState.IsValid)
            {
                baiviet.NgayTao = DateTime.Now;
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).Where(y => y.UserName == User.Identity.Name).Single().UserId;
                baiviet.UserProfileID = userid;
                //db.User.Identity.Name
                //Tao list các Tag
                List<Tag> Tags = new List<Tag>();
                //tach cac Tag theo dau ,
                string[] TagContent = Content.Split(',');
                //Lập các tag vừa tách
                foreach (string item in TagContent)
                {
                    //Tìm xem tag content đã có hay chưa
                    Tag TagExists = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //nếu có tag rồi thì add thêm Post vào
                        TagExists = ListTag.First();
                        TagExists.BaiViets.Add(baiviet);
                    }
                    else
                    {
                        //nếu chưa có tag thì tạo mới
                        TagExists = new Tag();
                        TagExists.Content = item;
                        TagExists.BaiViets = new List<BaiViet>();
                        TagExists.BaiViets.Add(baiviet);
                    }
                    //add vào List các tag
                    Tags.Add(TagExists);
                }
                //Gán List Tag cho Post
                baiviet.Tags = Tags;
                db.BaiViets.Add(baiviet);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(baiviet);
        }

        //
        // GET: /BaiViet/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // POST: /BaiViet/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BaiViet baiviet)
        {
            if (ModelState.IsValid)
            {
                db.Entry(baiviet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(baiviet);
        }

        //
        // GET: /BaiViet/Delete/5

        public ActionResult Delete(int id = 0)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            if (baiviet == null)
            {
                return HttpNotFound();
            }
            return View(baiviet);
        }

        //
        // POST: /BaiViet/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BaiViet baiviet = db.BaiViets.Find(id);
            db.BaiViets.Remove(baiviet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}