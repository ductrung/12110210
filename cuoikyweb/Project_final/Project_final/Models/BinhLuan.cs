﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_final.Models
{
    public class BinhLuan
    {
        public int ID { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String NoiDung { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.DateTime)]
        public DateTime NgayTao { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        public String TacGia { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - NgayTao).Minutes;
            }
        }
        public int BaiVietID { set; get; }
        public virtual BaiViet BaiViets { set; get; }
    }
}