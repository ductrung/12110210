﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_final.Models
{
    public class BaiViet
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 20-500", MinimumLength = 2)]
        public String TenBaiViet { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 20-500", MinimumLength = 2)]
        public String NoiDung { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime NgayTao { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileID { set; get; }
        //1 Post co nhieu comment
        public virtual ICollection<BinhLuan> BinhLuans { set; get; }
        //1 Post co nhieu Tag
        public virtual ICollection<Tag> Tags { set; get; }
        public virtual ICollection<DanhGia> DanhGias { set; get; }
        
    }
}