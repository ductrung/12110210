namespace Project_final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.BaiHocs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenBH = c.String(),
                        NoiDung = c.String(),
                        NgayDang = c.DateTime(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        LoaiBaiHocID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.LoaiBaiHocs", t => t.LoaiBaiHocID, cascadeDelete: true)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.LoaiBaiHocID);
            
            CreateTable(
                "dbo.LoaiBaiHocs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        LoaiBH = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.DanhGias",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BaiHocID = c.Int(nullable: false),
                        BaiVietID = c.Int(nullable: false),
                        Diem = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BaiHocs", t => t.BaiHocID, cascadeDelete: true)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .Index(t => t.BaiHocID)
                .Index(t => t.BaiVietID);
            
            CreateTable(
                "dbo.BaiViets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenBaiViet = c.String(nullable: false, maxLength: 500),
                        NoiDung = c.String(nullable: false, maxLength: 500),
                        NgayTao = c.DateTime(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
            CreateTable(
                "dbo.BinhLuans",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NoiDung = c.String(nullable: false, maxLength: 500),
                        NgayTao = c.DateTime(nullable: false),
                        TacGia = c.String(nullable: false),
                        BaiVietID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .Index(t => t.BaiVietID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Tag_BaiViet",
                c => new
                    {
                        BaiVietID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BaiVietID, t.TagID })
                .ForeignKey("dbo.BaiViets", t => t.BaiVietID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.BaiVietID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_BaiViet", new[] { "TagID" });
            DropIndex("dbo.Tag_BaiViet", new[] { "BaiVietID" });
            DropIndex("dbo.BinhLuans", new[] { "BaiVietID" });
            DropIndex("dbo.BaiViets", new[] { "UserProfile_UserId" });
            DropIndex("dbo.DanhGias", new[] { "BaiVietID" });
            DropIndex("dbo.DanhGias", new[] { "BaiHocID" });
            DropIndex("dbo.BaiHocs", new[] { "LoaiBaiHocID" });
            DropIndex("dbo.BaiHocs", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Tag_BaiViet", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_BaiViet", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.BinhLuans", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.BaiViets", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.DanhGias", "BaiVietID", "dbo.BaiViets");
            DropForeignKey("dbo.DanhGias", "BaiHocID", "dbo.BaiHocs");
            DropForeignKey("dbo.BaiHocs", "LoaiBaiHocID", "dbo.LoaiBaiHocs");
            DropForeignKey("dbo.BaiHocs", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.Tag_BaiViet");
            DropTable("dbo.Tags");
            DropTable("dbo.BinhLuans");
            DropTable("dbo.BaiViets");
            DropTable("dbo.DanhGias");
            DropTable("dbo.LoaiBaiHocs");
            DropTable("dbo.BaiHocs");
            DropTable("dbo.UserProfile");
        }
    }
}
