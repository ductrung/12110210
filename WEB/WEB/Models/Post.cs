﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QL_Account.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 20-500", MinimumLength = 2)]
        public String Title { set; get; }
        public String Body { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreate { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileID { set; get; }
        //1 Post co nhieu comment
        public virtual ICollection<Comment> Comments { set; get; }
        //1 Post co nhieu Tag
        public virtual ICollection<Tag> Tags { set; get; }
    }
}