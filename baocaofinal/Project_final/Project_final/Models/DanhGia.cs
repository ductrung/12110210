﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project_final.Models
{
    public class DanhGia
    {
        public int ID { set; get; }
        public int BaiHocID { set; get; }
        public virtual BaiHoc BaiHocs { set; get; }
        [Range (1,10,ErrorMessage="Diem tu 1 toi 10!!!")]
        public float? Diem { set; get; }

    }
}