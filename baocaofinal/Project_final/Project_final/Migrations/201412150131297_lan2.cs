namespace Project_final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BaiViets", "UserProfileUserId", "dbo.UserProfile");
            DropIndex("dbo.BaiViets", new[] { "UserProfileUserId" });
            RenameColumn(table: "dbo.BaiViets", name: "UserProfileUserId", newName: "UserProfile_UserId");
            AddColumn("dbo.BaiViets", "UserProfileID", c => c.Int(nullable: false));
            AddForeignKey("dbo.BaiViets", "UserProfile_UserId", "dbo.UserProfile", "UserId");
            CreateIndex("dbo.BaiViets", "UserProfile_UserId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BaiViets", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.BaiViets", "UserProfile_UserId", "dbo.UserProfile");
            DropColumn("dbo.BaiViets", "UserProfileID");
            RenameColumn(table: "dbo.BaiViets", name: "UserProfile_UserId", newName: "UserProfileUserId");
            CreateIndex("dbo.BaiViets", "UserProfileUserId");
            AddForeignKey("dbo.BaiViets", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: true);
        }
    }
}
