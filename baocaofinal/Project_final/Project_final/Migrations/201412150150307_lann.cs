namespace Project_final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lann : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DanhGias", "BaiVietID", "dbo.BaiViets");
            DropIndex("dbo.DanhGias", new[] { "BaiVietID" });
            AlterColumn("dbo.DanhGias", "Diem", c => c.Single());
            DropColumn("dbo.DanhGias", "BaiVietID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DanhGias", "BaiVietID", c => c.Int(nullable: false));
            AlterColumn("dbo.DanhGias", "Diem", c => c.Single(nullable: false));
            CreateIndex("dbo.DanhGias", "BaiVietID");
            AddForeignKey("dbo.DanhGias", "BaiVietID", "dbo.BaiViets", "ID", cascadeDelete: true);
        }
    }
}
