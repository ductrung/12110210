﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_final.Models;

namespace Project_final.Controllers
{
    public class DanhGiaController : Controller
    {
        private UsersContext db = new UsersContext();

        //
        // GET: /DanhGia/

        public ActionResult Index()
        {
            var danhgias = db.DanhGias.Include(d => d.BaiHocs);
            return View(danhgias.ToList());
        }

        //
        // GET: /DanhGia/Details/5

        public ActionResult Details(int id = 0)
        {
            DanhGia danhgia = db.DanhGias.Find(id);
            if (danhgia == null)
            {
                return HttpNotFound();
            }
            return View(danhgia);
        }

        //
        // GET: /DanhGia/Create

        public ActionResult Create()
        {
            ViewBag.BaiHocID = new SelectList(db.BaiHocs, "ID", "TenBH");
            return View();
        }

        //
        // POST: /DanhGia/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DanhGia danhgia, int idbaihoc)
        {
            if (ModelState.IsValid)
            {
                danhgia.BaiHocID = idbaihoc;
                db.DanhGias.Add(danhgia);
                db.SaveChanges();
                return RedirectToAction("Details/" + idbaihoc, "BaiHoc");
            }

            ViewBag.BaiHocID = new SelectList(db.BaiHocs, "ID", "TenBH", danhgia.BaiHocID);
            return View(danhgia);
        }

        //
        // GET: /DanhGia/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DanhGia danhgia = db.DanhGias.Find(id);
            if (danhgia == null)
            {
                return HttpNotFound();
            }
            ViewBag.BaiHocID = new SelectList(db.BaiHocs, "ID", "TenBH", danhgia.BaiHocID);
            return View(danhgia);
        }

        //
        // POST: /DanhGia/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DanhGia danhgia)
        {
            if (ModelState.IsValid)
            {
                db.Entry(danhgia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BaiHocID = new SelectList(db.BaiHocs, "ID", "TenBH", danhgia.BaiHocID);
            return View(danhgia);
        }

        //
        // GET: /DanhGia/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DanhGia danhgia = db.DanhGias.Find(id);
            if (danhgia == null)
            {
                return HttpNotFound();
            }
            return View(danhgia);
        }

        //
        // POST: /DanhGia/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DanhGia danhgia = db.DanhGias.Find(id);
            db.DanhGias.Remove(danhgia);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}