﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_final.Models
{
    public class LoaiBaiHoc
    {
        public int ID { set; get; }
        public String LoaiBH { set; get; }
        public virtual ICollection<BaiHoc> BaiHocs { set; get; }
    }
}