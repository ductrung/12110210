﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Project_final.Models
{
    public class BaiHoc
    {
        public int ID { set; get; }
        public String TenBH { set;get; }
        public String NoiDung { set; get; }
        public DateTime NgayDang { set; get; }
        public int UserProfileID { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        public int LoaiBaiHocID { set; get; }
        public virtual LoaiBaiHoc LoaiBaiHocs { set; get; }
        public virtual ICollection<DanhGia> DanhGias { set; get; }
    }
}